# Title
```
[2023-07-03] Work Stream | Lofi Music | Coding | Chill Stream
```

# Description
```
I am a freelancer developer and I work on my own hours but usually I work between 9am-5pm with breaks in between. If I need to, I sometimes work at night but mostly I just game and chill.

You can interact with me via chat. I'd like to have some conversation while working :)

https://www.juvarabrera/#/work
```

# Tags
```
work with me,study with me,lofi,chill,work,computer,programming,monitors,coding,javascript,Python,Java,JavaScript,C#,C++,Ruby,Go,Swift,PHP,Rust,Kotlin,TypeScript,R,Scala,Perl,MATLAB,Lua,Haskell,Objective-C,Groovy,JavaScript,Python,Ruby,Perl,PHP,Bash/Shell,PowerShell,Lua,Groovy,Tcl,AWK,REXX,VBScript,React,Angular,Vue.js,Ember.js,Backbone.js,Meteor.js,Svelte,Polymer,Aurelia,Knockout.js,Preact,Next.js,Gatsby.js,Nuxt.js,Alpine.js
```