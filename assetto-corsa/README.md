# Title
```
Chill Drive with a GTR on SLEX Skyway | Assetto Corsa, Track Day, Shutoko Revival Project Track
```
```
Driving a Ferrari on SLEX Skyway | Assetto Corsa, Track Day, Shutoko Revival Project Track
```

# Description
```
Game: Assetto Corsa
Car: 
Track: Shutoko Revival Project 0.9.1 (https://discord.gg/shutokorevivalproject)

[Setup]
Asus Vivobook 14 -It only has Intel i7 11th Gen @ 2.8GHz + Intel® Iris® Xe MAX Graphics
Logitech Brio - webcam
PXN V3II PRO Racing Wheel

#AssettoCorsa #Racing #Driving #Simulation
```

# Tags
```
Alfa Romeo 4C,Audi R8 LMS Ultra,BMW M3 E30,Ferrari 458 Italia,Lamborghini Huracán GT3,Lotus 98T,McLaren MP4-12C,Mercedes-Benz AMG GT3,Nissan GT-R Nismo GT3,Porsche 911 GT3 Cup,Subaru Impreza WRX STI,Toyota GT86,Assetto Corsa,Driving Game,Racing Game,Gaming,Steering Wheel,Gameplay,Simulation
```